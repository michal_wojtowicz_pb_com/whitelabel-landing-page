// =============================================================================
// Install node.js (http://nodejs.org/)
//
// Install Gulp task manager globally using command-line interface (CLI) of choice
// $ npm install -g gulp
//
// Navigate to project directory using CLI and install Gulp locally
// $ npm install --save-dev gulp
//
// Install all plugins defined in package.json
// $ npm install --save-dev
//
// =============================================================================


// Include Gulp
var gulp = require('gulp');

// Easy referencing of gulp- plugins from package.json
var plugins = require('gulp-load-plugins')();

// Additional plugin vars
var del = require('del');
var q = require('q');
var path = require('path');
var fs = require('fs');

// postCSS plugin vars
var postcss = require('gulp-postcss');
var cssnano = require('cssnano');

// folder vars
var dest = "./dist";
var src = './src';


// Concatenate & Minify JS
// $ gulp scripts
// =============================================================================
gulp.task('scripts', function () {
  // Consider the "source" order of script references
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    src + '/js/*.js'
  ])
    .pipe(plugins.concat('script.js'))
    .pipe(plugins.uglify())
    .pipe(gulp.dest(dest + '/js'))
    .pipe(plugins.notify({
      message: 'Scripts task complete',
      onLast: true
    }));
});


// Compile & Minify CSS
// $ gulp styles
// =============================================================================
gulp.task('styles', ['bootstrap', 'ie7'], function () {

  var onError = function (err) {
    plugins.notify.onError({
      title: "Gulp",
      subtitle: "Failure!",
      message: "Error: <%= error.message %>",
      sound: "Beep"
    })(err);

    this.emit('end');
  };

  // Post-CSS processors
  var processors = [
    cssnano
  ];

  // Reference the CSS files
  return gulp.src([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    src + '/css/*.css',
  ])
    .pipe(plugins.plumber({
      errorHandler: onError
    }))
    .pipe(plugins.concat('style.css'))
    .pipe(postcss(processors))
    .pipe(gulp.dest(dest + '/css'))

  // Notify when task has fully completed
  .pipe(plugins.notify({
    message: 'CSS task complete',
    onLast: true
  }));
});


// Handle the bootstrap css min file to avoid console warnings
gulp.task('bootstrap', function () {
  return gulp.src([
    'node_modules/bootstrap/dist/css/bootstrap.min.css.map'
  ])
    .pipe(gulp.dest(dest + '/css'));
});


// Move the IE7 stylesheet to the dist folder
gulp.task('ie7', function () {
  return gulp.src([
    src + '/css-ie7/*.css'
  ])
    .pipe(gulp.dest(dest + '/css'))
});


// Optimise images (once)
// $ gulp images
// =============================================================================
gulp.task('images', function () {
  return gulp.src(src + '/images/**/*.*')
    .pipe(plugins.cache(plugins.imagemin({
      optimizationLevel: 5,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest(dest + '/images'))
    .pipe(plugins.notify({
      message: 'Images task complete',
      onLast: true
    }));
});


// Move custom fonts to the dist folder
// $ gulp fonts
// =============================================================================
gulp.task('fonts', function () {
  return gulp.src([
    src + '/fonts/**/*'
  ])
    .pipe(gulp.dest(dest + '/fonts'))
    .pipe(plugins.notify({
      message: 'fonts copied to dist folder',
      onLast: true
    }));
});


// Delete the dist folder and start over
// $ gulp clean
// =============================================================================
gulp.task('clean', function (cb) {
  del('dist', cb);
});


// Run all gulp tasks
// $ gulp
// =============================================================================
gulp.task('default', function () {
  gulp.start('scripts', 'images', 'styles', 'fonts');
});


// Watch files for changes and run associated gulp task
// $ gulp watch
// =============================================================================
gulp.task('watch', function () {
  gulp.run('default');

  gulp.watch('src/js/*.js', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    gulp.run('scripts');
  });

  gulp.watch('src/images/**/*', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    gulp.run('images');
  });

  gulp.watch('src/fonts/**/*', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    gulp.run('fonts');
  });

  gulp.watch('src/css/*.css', function (event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    gulp.run('styles');
  });
});
