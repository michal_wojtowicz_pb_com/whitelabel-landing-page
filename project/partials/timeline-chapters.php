<div id="jsBufferTimeline" class="buffer-container col-xs-12 nopad"></div>

<div id="jsTimelineProgressHover" class="timeline-progress-hover"></div>
<div id="jsTimelineProgress" class="timeline-progress">
    <div id="jsTimelineIndicator" class="timeline-indicator"></div>
</div>

<div id="jsTimelineContainer" class="col-xs-12 timeline-chapter-buttons nopad">

    <div id="jsNormalTimeline">
        <div class="chapter-button chapter-button__introduction">
            <!-- This chapter button only has one state associated with it -->
            <div class="timeline-state" data-percent-width="20" data-percent-start="0" data-state="introduction"></div>
            <span class="translate" data-translate="ChapterIntroduction">Introduction</span>
        </div>

        <div class="chapter-button chapter-button__chapter1">
            <!-- This chapter button has two states associated with it -->
            <div class="timeline-state" data-percent-width="10" data-percent-start="20" data-state="chapter1"></div>
            <div class="timeline-state" data-percent-width="10" data-percent-start="30" data-state="chapter1-pt2"></div>
            <span class="translate" data-translate="Chapter1">Chapter 1</span>
        </div>

        <div class="chapter-button chapter-button__chapter2">
            <div class="timeline-state" data-percent-width="10" data-percent-start="40" data-state="chapter2"></div>
            <span class="translate" data-translate="Chapter2">Chapter 2</span>
        </div>

        <div class="chapter-button chapter-button__chapter3">
            <div class="timeline-state" data-percent-width="20" data-percent-start="50" data-state="chapter3"></div>
            <span class="translate" data-translate="Chapter3">Chapter 3</span>
        </div>

        <div class="chapter-button chapter-button__chapter4">
            <div class="timeline-state" data-percent-width="15" data-percent-start="70" data-state="chapter4"></div>
            <span class="translate" data-translate="Chapter4">Chapter 4</span>
        </div>

        <div class="chapter-button chapter-button__thanks">
            <div class="timeline-state" data-percent-width="15" data-percent-start="85" data-state="thanks"></div>
            <span class="translate" data-translate="ChapterThanks">Thanks</span>
        </div>

    </div>

</div>
